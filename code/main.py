import pandas as pd

from code.evaluation import rebuild_kfold_sets
from code.prepare_data import COLUMNS, DataPreparation
from code.training.fast_text_classifier import FastTextClassifier

df = DataPreparation("./data/datasets/jirasoftware.csv").get_data_frame()

# K-folds cross validation
# K=5 or K=10 are generally used.
# Note that the overall execution time increases linearly with k
k = 5

# Make Dataset random before start
df_rand = df.sample(df.storypoint.count(), random_state=99)

# Number of examples in each fold
fsamples = int(df_rand.storypoint.count() / k)

# Fill folds (obs: last folder could contain less than fsamples datapoints)
folds = list()
for i in range(k):
    folds.append(df_rand.iloc[i * fsamples: (i + 1) * fsamples])

# Init
error_list = []

# Repeat k times and average results
for i in range(k):
    error = 0
    # Build new training and testing set for iteration i
    training_set, testing_set = rebuild_kfold_sets(folds, k, i)
    y_true = testing_set.storypoint.tolist()

    # Oversample (ONLY TRAINING DATA)
    # X_resampled, y_resampled = DataPreparation.simple_over_sample(training_set.label_title_desc.values.tolist(),
    #                                                              training_set.storypoint.values.tolist())

    # Downsample (ONLY TRAINING DATA)
    X_resampled, y_resampled = DataPreparation.simple_down_sample(training_set)

    # 3 - Train
    clf = FastTextClassifier()
    clf.fit(X_resampled)

    # Predict
    y_pred = clf.predict(testing_set.label_title_desc.values.tolist())

    # Update Overall Accuracy
    data = []
    for num_pred in range(len(y_pred)):
        error += abs(y_pred[num_pred] - y_true[num_pred])
        data.append([y_true[num_pred], y_pred[num_pred]])

    results_df = pd.DataFrame(data, columns=['True value', 'Predicted Value'])
    results_df.to_csv('./data/results/appceleratorstudio_results_{}.csv'.format(i), index=False)

    error_list.append(error)
    print(error)
