# README #

### ¿Qué es SoftEstim? ###

SoftEstim utiliza algoritmo de aprendizaje profundo para predecir el coste
y esfuerzo de historias de usuario relacionadas con el desarrollo software.
En este repositorio se encuentra el código para:
- Preparar los datos
- Pre-entrenar el modelo
- Entrenar el modelo
- Predecir
- Evaluar el modelo

### Dependencias ###

Esta aplicación está disponible solo para Linux (se ha probado en 
Ubuntu 18.04, pero otras distribuciones también podrían servir); 
los paquetes y herramientas necesarios para ejecutarla son:

- git (sudo apt-get install git)
- python (Version 3.6)
- El resto de requisitos se encuentran en el fichero Pipfile

### Clonar el repositorio ###
En un terminal, moverse al directorio donde se desea guardar el proyecto, 
y clonar el repositorio ejecutando el comando:

> git clone https://jenniferhurtado@bitbucket.org/jenniferhurtado/softestim.git

### Preparar el entorno ###
En un terminal, moverse al directorio en el que se ha clonado el 
repositorio, e instalar los requerimientos del Pipfile. Se recomienda
instalar los paquetes en un entorno virtual

> pipenv install

### Ejecución ###
Para ejecutar el código, abrimos un terminar python. Se recomienda
utilizar ipython

> ipython

#### Pre-procesar los datos ####

- Copiar el código de prepare_data.py
- Instanciar el objecto DataPreparation
> data_preparation = DataPreparation(path_to_dataset)

El parámetro path_to_dataset debe ser la ruta al fichero
csv que queremos preparar. Para que el código funcione correctamente, 
las columnas del fichero deben coincidir con la constante COLUMNS de la 
clase DataPreparation. 

- Obtener el dataset pre-procesado
> preprocessed_df = data_preparation.get_data_frame()

#### Pre-entrenar el modelo ####
- Copiar el código de pretraining.py

Esto ejecutará el preprocesamiento de los datasets especificados en la 
variable pretrain_files. Para cambiar los datasets con los que pre-entrenar,
se deben cambiar las rutas de los ficheros en esta variable.

El resultado será un fichero llamado pretrain_model, que contendrá
el modelo pre-entrenado

#### Entrenar el modelo y predecir ####
- Copiar el código de fast_text_classifier.py
- Instanciar el objecto FastTextClassifier
> classifier = FastTextClassifier()

- Entrenar el modelo
> classifier.fit(xtrain)

El parámetro xtrain debe ser el dataset con el que se va a entrenar. El
resultado será un fichero cuyo nombre empieza por supervised_classifier_model_ 

- Predecir
> prediction = classifier.predict(xtest)

El parámetro xtet debe ser el dataset que se quiere predecir, y por tanto
no debe tener la columna a estimar. La función devuelve el dataset con
la columna a predecir rellena

#### Evaluar ####
- Copiar el código de main.py

Este fichero toma el dataset especificado en la línea 7, lo pre-procesa,
entrena un modelo utilizando el fichero del pre-entrenamiento, predice, y 
evalua la precisión.

